package demo.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

/**
 * Created by MASTAH on 2019-04-05.
 */
@DefaultUrl("http://automationpractice.com/index.php?controller=authentication&back=my-account")
public class IPFRegistationPage extends IPFPage {

    @FindBy(id = "email_create", timeoutInSeconds="10")
    WebElementFacade emailInput;

    @FindBy(id = "SubmitCreate")
    WebElementFacade createAccountButton;

    public IPFRegistationPage(WebDriver driver) {
        super(driver);
    }

    public void displayRegistrationForm(){
        open();
    }

    public void fillEmail(String email){
        emailInput.clear();
        emailInput.sendKeys(email);
    }

    public void clickCreateAccountButton(){
        createAccountButton.click();
    }

    public boolean isFullFormVisible(){
        return !getDriver().findElements(By.id("account-creation_form")).isEmpty();
    }

    public boolean isErrorVisible(){
        getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        boolean result = !getDriver().findElements(By.cssSelector("#create_account_error ol li")).isEmpty();
        getDriver().manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        return result;
    }

    public void selectTitle(int title){
        getDriver().findElement(By.id("id_gender" + title)).click();
    }

    public void fillFirstName(String firstName){
        getDriver().findElement(By.id("customer_firstname")).sendKeys(firstName);
    }

    public void fillLastName(String lastName){
        getDriver().findElement(By.id("customer_lastname")).sendKeys(lastName);
    }

    public void fillPassword(String password){
        getDriver().findElement(By.id("passwd")).sendKeys(password);
    }

    public void selectDateOfBirth(int day, int month, int year){
        getDriver().findElement(By.cssSelector("#days option[value=\"" + day + "\"]")).click();
        getDriver().findElement(By.cssSelector("#months option[value=\"" + month + "\"]")).click();
        getDriver().findElement(By.cssSelector("#years option[value=\"" + year + "\"]")).click();
    }

    public void fillAddressFirstName(String firstName){
        getDriver().findElement(By.id("firstname")).sendKeys(firstName);
    }

    public void fillAddressLastName(String lastName){
        getDriver().findElement(By.id("lastname")).sendKeys(lastName);
    }

    public void fillAddress(String address){
        getDriver().findElement(By.id("address1")).sendKeys(address);
    }

    public void fillCity(String city){
        getDriver().findElement(By.id("city")).sendKeys(city);
    }

    public void selectCountry(String country){
        getDriver().findElement(By.cssSelector("#id_country option[value=\"" +country + "\"")).click();
    }

    public void selectState(int state){
        getDriver().findElement(By.cssSelector("#id_state option[value=\"" +state + "\"")).click();
    }

    public void fillZipCode(String zipcode){
        getDriver().findElement(By.id("postcode")).sendKeys(zipcode);
    }

    public void fillPhoneNumber(String phone){
        getDriver().findElement(By.id("phone_mobile")).sendKeys(phone);
    }

    public void fillAddressAlias(String addressAlias){
        WebElement addressAliasElement = getDriver().findElement(By.id("alias"));
        addressAliasElement.clear();
        addressAliasElement.sendKeys(addressAlias);
    }

    public IPFMainPage submitRegistrationForm(){
        getDriver().findElement(By.id("submitAccount")).click();
        return switchToPage(IPFMainPage.class);
    }

    public boolean isRegistrationErrorVisible(){
        return !getDriver().findElements(By.cssSelector("#authentication .alert-danger ol li")).isEmpty();
    }

}
