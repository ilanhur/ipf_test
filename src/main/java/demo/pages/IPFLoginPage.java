package demo.pages;

import demo.model.IPFNewUserModel;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebDriver;

/**
 * Created by MASTAH on 2019-04-05.
 */
@DefaultUrl("http://automationpractice.com/index.php?controller=authentication&back=my-account")
public class IPFLoginPage extends IPFPage {
    @FindBy(id = "email", timeoutInSeconds="10")
    WebElementFacade emailInput;

    @FindBy(id = "passwd")
    WebElementFacade passwordInput;

    @FindBy(id = "SubmitLogin")
    WebElementFacade loginButton;


    public IPFLoginPage(WebDriver driver) {
        super(driver);
    }

    public void fillEmail(String email){
        emailInput.clear();
        emailInput.sendKeys(email);
    }

    public void fillPassword(String password){
        passwordInput.clear();
        passwordInput.sendKeys(password);
    }

    public IPFMainPage clickLoginButton(){
        loginButton.click();
        return switchToPage(IPFMainPage.class);
    }


}

