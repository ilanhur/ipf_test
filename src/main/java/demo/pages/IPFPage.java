package demo.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebDriver;

/**
 * Created by MASTAH on 2019-04-05.
 */

@DefaultUrl("http://automationpractice.com/index.php")
public class IPFPage extends PageObject {
    @FindBy(css = "a.logout", timeoutInSeconds="10")
    WebElementFacade logoutLink;

    public IPFPage(WebDriver driver) {
        super(driver);
    }

    public void logout(){
        logoutLink.click();
    }
}
