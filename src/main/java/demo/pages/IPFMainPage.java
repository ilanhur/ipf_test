package demo.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebDriver;

/**
 * Created by MASTAH on 2019-04-05.
 */
@DefaultUrl("http://automationpractice.com/index.php")
public class IPFMainPage extends IPFPage {

    public IPFMainPage(WebDriver driver) {
        super(driver);
    }


    public boolean isUserLoggedIn(){
        return !getDriver().findElements(By.id("my-account")).isEmpty();
    }
}
