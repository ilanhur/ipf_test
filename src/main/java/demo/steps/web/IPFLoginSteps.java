package demo.steps.web;

import demo.model.IPFNewUserModel;
import demo.pages.IPFLoginPage;
import demo.pages.IPFMainPage;
import demo.pages.IPFRegistationPage;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Created by MASTAH on 2019-04-05.
 */
public class IPFLoginSteps {

    IPFLoginPage loginPage;

    public void login(IPFNewUserModel userModel){
        loginPage.open();
        loginPage.fillEmail(userModel.getEmail());
        loginPage.fillPassword(userModel.getPassword());
        IPFMainPage mainPage = loginPage.clickLoginButton();
        assertThat("User is not logged in", mainPage.isUserLoggedIn(), is(true));
    }


}
