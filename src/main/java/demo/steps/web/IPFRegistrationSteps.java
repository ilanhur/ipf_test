package demo.steps.web;

import demo.model.IPFNewUserModel;
import demo.pages.IPFMainPage;
import demo.pages.IPFRegistationPage;
import demo.steps.core.CoreStep;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by MASTAH on 2019-04-05.
 */
public class IPFRegistrationSteps extends CoreStep {

    IPFRegistationPage registrationPage;

    public void creatUser(IPFNewUserModel userModel){
        fillInitialForm(userModel);
        verifyValidationPassed();
        IPFMainPage mainPage = fillRegistrationForm(userModel);
        assertThat("User is not logged in", mainPage.isUserLoggedIn(), is(true));
        mainPage.logout();
    }

    public void fillInitialForm(IPFNewUserModel userModel){
        registrationPage.displayRegistrationForm();
        registrationPage.fillEmail(userModel.getEmail());
        registrationPage.clickCreateAccountButton();
    }

    public void verifyValidationPassed(){
        assertThat("There are errors on registration form", registrationPage.isErrorVisible(), is(false));
        assertThat("Full registration form is not displayed", registrationPage.isFullFormVisible(), is(true));
    }

    public void verifyValidationFailed(){
        assertThat("There are no errors on registration form", registrationPage.isErrorVisible(), is(true));
        assertThat("Full registration form is displayed", registrationPage.isFullFormVisible(), is(false));
    }

    public void verifyRegistrationFormHasErrors(){
        assertThat("Full registration form has no errors", registrationPage.isRegistrationErrorVisible(), is(true));
    }

    public void verifyRegistrationFormHasNoErrors(){
        assertThat("Full registration form has errors", registrationPage.isRegistrationErrorVisible(), is(false));
    }

    public IPFMainPage fillRegistrationForm(IPFNewUserModel userModel){
        registrationPage.selectTitle(userModel.getTitle());
        registrationPage.fillFirstName(userModel.getFirstName());
        //registrationPage.fillAddressFirstName(userModel.getFirstName());
        registrationPage.fillLastName(userModel.getLastName());
        //registrationPage.fillAddressLastName(userModel.getLastName());
        registrationPage.fillPassword(userModel.getPassword());
        registrationPage.selectDateOfBirth(
                userModel.getDayOfBirth(),
                userModel.getMonthOfBirth(),
                userModel.getYearOfBirth());
        registrationPage.fillAddress(userModel.getAddress());
        registrationPage.selectCountry(userModel.getCountry());
        registrationPage.selectState(userModel.getState());
        registrationPage.fillZipCode(userModel.getZipcode());
        registrationPage.fillCity(userModel.getCity());
        registrationPage.fillPhoneNumber(userModel.getMobilePhone());
        registrationPage.fillAddressAlias(userModel.getAddressAlias());
        return registrationPage.submitRegistrationForm();
    }

    public void checkEmptyEmailValidation(){
        IPFNewUserModel userModel = new IPFNewUserModel();
        userModel.setEmail("");
        fillInitialForm(userModel);
        verifyValidationFailed();
    }

    public void checkStringAsEmailValidation(){
        IPFNewUserModel userModel = new IPFNewUserModel();
        userModel.setEmail("notanemail");
        fillInitialForm(userModel);
        verifyValidationFailed();
    }

    public void checkDoubleAtInEmailValidation(){
        IPFNewUserModel userModel = new IPFNewUserModel();
        userModel.setEmail("wrong@email@wrong.com");
        fillInitialForm(userModel);
        verifyValidationFailed();
    }

    public void checkSpecialCharsInEmailValidation(){
        IPFNewUserModel userModel = new IPFNewUserModel();
        userModel.setEmail("wrong\"email@wrong.com");
        fillInitialForm(userModel);
        verifyValidationFailed();
    }

    public void checkEmptyPasswordValidation(){
        IPFNewUserModel userModel = new IPFNewUserModel();
        userModel.setPassword("");
        jumpToFullForm(userModel);
    }

    public void checkTooShortPasswordValidation(){
        IPFNewUserModel userModel = new IPFNewUserModel();
        userModel.setPassword("1234");
        jumpToFullForm(userModel);
    }

    private void jumpToFullForm(IPFNewUserModel userModel){
        fillInitialForm(userModel);
        verifyValidationPassed();
        fillRegistrationForm(userModel);
        verifyRegistrationFormHasErrors();
    }

//    private OlxSearchPage onOlxSearchPage() {
//        return getPages().get(OlxSearchPage.class);
//    }
//
//    private OlxSearchResultPage onOlxSearchResultPage() {
//        return getPages().get(OlxSearchResultPage.class);
//    }
}
