package demo.steps.core;

import net.thucydides.core.annotations.Step;

/**
 * Class used as a stub/parent for all steps.
 * Should contain methods shared often throught project.
 * Should not grow into GoldenHammer!
 */
public class CoreStep {

    /**
     * Method used to add log elements into report.
     * The String passed in will be dispalyed as step in the.
     * @param log
     */
    @Step("{0}")
    public void addLog(String log){}
}
