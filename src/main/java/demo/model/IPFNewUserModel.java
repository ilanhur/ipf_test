package demo.model;

import org.apache.commons.lang3.RandomStringUtils;


/**
 * Created by MASTAH on 2019-04-05.
 */
public class IPFNewUserModel {

    private String email;
    private int title = 1;
    private String firstName = "Tester";
    private String lastName = "Tester";
    private String password;
    private int dayOfBirth = 1;
    private int monthOfBirth = 1;
    private int yearOfBirth = 1970;
    private String addressFirstName = "Tester";
    private String addressLastName = "Tester";
    private String address = "some not important address 1";
    private String city = "Lodz";
    private int state = 1;
    private String zipcode = "00000";
    private String country = "21";
    private String mobilePhone = "123456789";
    private String addressAlias = "someName";

    public IPFNewUserModel(){
        setRandomEmail();
        setRandomPassword();
    }

    public IPFNewUserModel(String email){
        setEmail(email);
        setRandomPassword();
    }

    public String getEmail() {
        return email;
    }

    public IPFNewUserModel setRandomEmail() {
        this.email = RandomStringUtils.randomAlphabetic(8) + "@test.pl";
        return this;
    }

    public IPFNewUserModel setEmail(String email) {
        this.email = email;
        return this;
    }

    public int getTitle() {
        return title;
    }

    public IPFNewUserModel setTitle(int title) {
        this.title = title;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public IPFNewUserModel setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public IPFNewUserModel setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public IPFNewUserModel setPassword(String password) {
        this.password = password;
        return this;
    }

    public IPFNewUserModel setRandomPassword() {
        this.password = RandomStringUtils.randomAlphabetic(8);
        return this;
    }

    public int getDayOfBirth() {
        return dayOfBirth;
    }

    public IPFNewUserModel setDayOfBirth(int dayOfBirth) {
        this.dayOfBirth = dayOfBirth;
        return this;
    }

    public int getMonthOfBirth() {
        return monthOfBirth;
    }

    public IPFNewUserModel setMonthOfBirth(int monthOfBirth) {
        this.monthOfBirth = monthOfBirth;
        return this;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public IPFNewUserModel setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
        return this;
    }

    public String getAddressFirstName() {
        return addressFirstName;
    }

    public IPFNewUserModel setAddressFirstName(String addressFirstName) {
        this.addressFirstName = addressFirstName;
        return this;
    }

    public String getAddressLastName() {
        return addressLastName;
    }

    public IPFNewUserModel setAddressLastName(String addressLastName) {
        this.addressLastName = addressLastName;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public IPFNewUserModel setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getCity() {
        return city;
    }

    public IPFNewUserModel setCity(String city) {
        this.city = city;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public IPFNewUserModel setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public IPFNewUserModel setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
        return this;
    }

    public String getAddressAlias() {
        return addressAlias;
    }

    public IPFNewUserModel setAddressAlias(String addressAlias) {
        this.addressAlias = addressAlias;
        return this;
    }

    public int getState() {
        return state;
    }

    public IPFNewUserModel setState(int state) {
        this.state = state;
        return this;
    }

    public String getZipcode() {
        return zipcode;
    }

    public IPFNewUserModel setZipcode(String zipcode) {
        this.zipcode = zipcode;
        return this;
    }
}
