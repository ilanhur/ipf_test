package demo.model;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebDriver;

/**
 * Created by MASTAH on 2019-04-05.
 */
@DefaultUrl("http://automationpractice.com/index.php?controller=authentication&back=my-account")
public class IPFApiModel extends PageObject {

        @FindBy(id = "email_create", timeoutInSeconds="10")
        WebElementFacade emailInput;

        @FindBy(id = "SubmitCreate")
        WebElementFacade createAccountButton;

        public IPFApiModel(WebDriver driver) {
            super(driver);
        }

        public void displayRegistrationForm(){
            open();
        }

        public void enterEmailAddress(String email){
            emailInput.clear();
            emailInput.sendKeys(email);
        }

        public void cliclCreateAccountButton(){
            createAccountButton.click();
        }


    }
