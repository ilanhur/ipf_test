package demo.suites.web;

        import demo.listeners.WebDriverListener;
        import demo.model.IPFNewUserModel;
        import demo.steps.web.IPFRegistrationSteps;
        import net.serenitybdd.junit.runners.SerenityRunner;
        import net.thucydides.core.annotations.Narrative;
        import net.thucydides.core.annotations.Pending;
        import net.thucydides.core.annotations.Steps;
        import net.thucydides.core.annotations.Title;
        import org.junit.Test;
        import org.junit.runner.RunWith;


@RunWith(SerenityRunner.class)
@Narrative(text={"Test checking if it`s possible to search via olx"})
public class IPFRegisterTest extends WebDriverListener {

    @Steps
    IPFRegistrationSteps registrationSteps;


    @Test
    @Title("Check email validation for common input errors")
    public void checkIfEmailValidationIsCheckingForCommonInputErrors(){
        registrationSteps.checkEmptyEmailValidation();
        registrationSteps.checkStringAsEmailValidation();
        registrationSteps.checkDoubleAtInEmailValidation();
        registrationSteps.checkSpecialCharsInEmailValidation();
    }

    @Test
    @Title("Check password validation for common input errors")
    public void checkPasswordValidationForCommonInputErrors(){
        registrationSteps.checkEmptyPasswordValidation();
        registrationSteps.checkTooShortPasswordValidation();
    }

    @Test
    @Title("Check if system is checking if email is already in use")
    @Pending
    public void checkIfSystemIsCheckingIfEmailIsAlreadyInUse(){
        IPFNewUserModel userModel = new IPFNewUserModel();
        registrationSteps.creatUser(userModel);
    }

}
