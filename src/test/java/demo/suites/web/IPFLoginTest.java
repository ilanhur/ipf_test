package demo.suites.web;

import demo.listeners.WebDriverListener;
import demo.model.IPFNewUserModel;
import demo.pages.IPFLoginPage;
import demo.steps.web.IPFLoginSteps;
import demo.steps.web.IPFRegistrationSteps;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by MASTAH on 2019-04-05.
 */

@RunWith(SerenityRunner.class)
public class IPFLoginTest  extends WebDriverListener {

    @Steps
    IPFRegistrationSteps registrationSteps;

    @Steps
    IPFLoginSteps loginSteps;

    @Before
    public void prepareTest(){
        IPFNewUserModel userModel = new IPFNewUserModel();
        registrationSteps.creatUser(userModel);
        Serenity.setSessionVariable("UserModel").to(userModel);
    }

    @Test
    @Title("Check if created user is able to log in")
    public void checkIfExistingUserCanLogIn(){
        IPFNewUserModel userModel = Serenity.sessionVariableCalled("UserModel");
        loginSteps.login(userModel);
    }

}
