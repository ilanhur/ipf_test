Feature: Checking withdrawls from account

  We need to check if when user withdraws money from his account
  then this change is reflected in account balance

  Scenario: Should not allow user to withdraw more money than he has
    Given user has 200 PLN on his account
    When user tries to withdraw 500 PLN
    Then an error message should appear
    And no money should be given to the user
    And user should have 200 PLN on his account

  Scenario: Should allow user to withdraw money
    Given user has 200 PLN on his account
    When user tries to withdraw <Amount> PLN
    Then no error message should appear
    And <Amount> PLN should be given to the user
    And user should have 200 PLN - <Amount> on his account
    Examples:
    | Amount |
    | 0      |
    | 50     |
    | 100    |
    | 150    |
    | 200    |