package demo.listeners;

import org.junit.AfterClass;
import org.junit.BeforeClass;

/**
 * Standard listener used to prepare/teardown environment
 * with suites that do not require WebDriver.
 */
public class SuiteListener {

    @BeforeClass
    public static void prepareSuite(){

    }

    @AfterClass
    public static void teardownSuite(){

    }
}
